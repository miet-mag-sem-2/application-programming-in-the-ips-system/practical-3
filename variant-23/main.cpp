// Serial Program
#include <stdio.h>
#include <windows.h>
#include <mmsystem.h>
#include <math.h>

#include <chrono>

using namespace std;

#define OPENMP

//const long int VERYBIG = 100000;
const long int VERYBIG = 10000;
// ***********************************************************************
int main(void)
{
    int i;
    long int j, k, sum;
    double sumx, sumy, total;
    //DWORD starttime, elapsedtime;
    // -----------------------------------------------------------------------
      // Output a start message
    printf("None Parallel Timings for %ld iterations\n\n", VERYBIG);

    // repeat experiment several times
    for (i = 0; i < 6; i++)
    {
        // get starting time
        //starttime = timeGetTime(); 
        chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

        // reset check sum & running total
        sum = 0;
        total = 0.0;
        // Work Loop, do some work by looping VERYBIG times
#ifdef OPENMP 
    #pragma omp parallel for reduction (+:sum, total) private(sumx, sumy)
#endif
        for (j = 0; j < VERYBIG; j++)
        {
            // increment check sum
            sum += 1;

            // Calculate first arithmetic series
            sumx = 0.0;
            sumy = 0.0;

#ifdef OPENMP
    #pragma omp parallel for reduction (+:sumx, sumy)
#endif
            for (k = 0; k < j; k++) {
                sumx = sumx + (double)k;
                sumy = sumy + (double)(j - k);
            }

            // Calculate second arithmetic series
//            sumy = 0.0;
//#ifdef OPENMP
//    #pragma omp parallel for reduction (+:sumy)
//#endif
//            for (k = j; k > 0; k--)
//                sumy = sumy + (double)k;

            if (sumx > 0.0)total = total + 1.0 / sqrt(sumx);
            if (sumy > 0.0)total = total + 1.0 / sqrt(sumy);
        }

        // get ending time and use it to determine elapsed time
        //elapsedtime = timeGetTime() - starttime;
        chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
        chrono::duration<double> duration = (t2 - t1);

        // report elapsed time
        printf("Time Elapsed %10.3f Secs Total=%lf Check Sum = %ld\n",
            duration.count(), total, sum);
    }

    // return integer as required by function header
    return 0;
}
// **********************************************************************